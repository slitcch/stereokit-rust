

pub type bool32_t = i32;
pub const display_mode__display_mode_mixedreality: display_mode_ = 0;
pub const display_mode__display_mode_flatscreen: display_mode_ = 1;
pub const display_mode__display_mode_none: display_mode_ = 2;

#[doc = ""]
pub type display_mode_ = ::std::os::raw::c_uint;
pub const depth_mode__depth_mode_balanced: depth_mode_ = 0;
pub const depth_mode__depth_mode_d16: depth_mode_ = 1;
pub const depth_mode__depth_mode_d32: depth_mode_ = 2;
pub const depth_mode__depth_mode_stencil: depth_mode_ = 3;
pub type depth_mode_ = ::std::os::raw::c_uint;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct sk_settings_t {
    pub app_name: *const ::std::os::raw::c_char,
    pub assets_folder: *const ::std::os::raw::c_char,
    pub display_preference: display_mode_,
    pub no_flatscreen_fallback: bool32_t,
    pub depth_mode: depth_mode_,
    pub flatscreen_pos_x: i32,
    pub flatscreen_pos_y: i32,
    pub flatscreen_width: i32,
    pub flatscreen_height: i32,
    pub disable_flatscreen_mr_sim: bool32_t,
    pub android_java_vm: *mut ::std::os::raw::c_void,
    pub android_activity: *mut ::std::os::raw::c_void,
}


impl Default for sk_settings_t {
	fn default() -> Self {
		let mut name_rust = "Hello from Rust!!";
		let name_c = std::ffi::CString::new(name_rust).unwrap();
		let assets_c = std::ffi::CString::new(name_rust).unwrap();


		sk_settings_t {
			app_name: name_c.into_raw(),
			assets_folder: assets_c.into_raw(),
			display_preference: 0,
			no_flatscreen_fallback: 0,
			depth_mode: depth_mode__depth_mode_d16,
			flatscreen_pos_x: 0,
			flatscreen_pos_y: 0,
			flatscreen_width: 1280,
			flatscreen_height: 720,
			disable_flatscreen_mr_sim: 0,
			android_java_vm: ::std::ptr::null_mut(),
			android_activity: ::std::ptr::null_mut(),
		}
	}
}

extern "C" {
	pub fn sk_init(settings: sk_settings_t) -> bool32_t;
}